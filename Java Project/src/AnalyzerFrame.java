import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.Date;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;

public class AnalyzerFrame extends JFrame
{

	private final String VERSION = "File Analyzer v1.0";
    private Component MainFrame;
    private TextFileHistory history;
    private TextFile current;
    private JLabel currentFileData;
    private JLabel aggregateFileData;
    private JLabel fileHistory;
    
    public AnalyzerFrame()
    {
        super();
        MainFrame = this;
        this.setTitle(VERSION);
        history = new TextFileHistory();
        
        JPanel containerPanel= new JPanel();
        containerPanel.setLayout(new BorderLayout());
        containerPanel.setBorder(new EmptyBorder(10,0,10,0));
        add(containerPanel);
        
        JPanel buttonPanel=new JPanel();
        buttonPanel.setLayout(new BorderLayout());
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("*.txt", "txt"));
        JButton chooseButton = new JButton("Choose a file");
        JButton closeButton = new JButton("Close");
        JButton helpButton = new JButton("Help");
        JLabel title = new JLabel("File Analyzer", SwingConstants.CENTER);
        title.setFont(title.getFont().deriveFont(Font.BOLD, 14f));
        buttonPanel.add(chooseButton, BorderLayout.PAGE_END);
        buttonPanel.add(helpButton, BorderLayout.LINE_START);
        buttonPanel.add(closeButton, BorderLayout.LINE_END);
        buttonPanel.add(title, BorderLayout.CENTER);
        
        JPanel dataPanel=new JPanel();
        dataPanel.setLayout(new GridLayout(1,2));
        dataPanel.setBorder(new EmptyBorder(10,10,10,10));
        JPanel currentData=new JPanel();
        JPanel aggregateData=new JPanel();
        JLabel currentTitle = new JLabel("Current File");
        currentTitle.setFont(currentTitle.getFont().deriveFont(Font.BOLD, 13f));
        JLabel aggregateTitle = new JLabel("Average Across Files");
        aggregateTitle.setFont(aggregateTitle.getFont().deriveFont(Font.BOLD, 13f));
        currentFileData = new JLabel();
        aggregateFileData = new JLabel();
        currentData.setLayout(new BoxLayout(currentData, BoxLayout.PAGE_AXIS));
        currentData.add(currentTitle);
        currentData.add(currentFileData);
        aggregateData.setLayout(new BoxLayout(aggregateData, BoxLayout.PAGE_AXIS));
        aggregateData.add(aggregateTitle);
        aggregateData.add(aggregateFileData);
        dataPanel.add(currentData);
        dataPanel.add(aggregateData);
        updateCurrentData();
        updateAggregateData();
        
        JPanel fileHistoryPanel = new JPanel();
        fileHistoryPanel.setBorder(new EmptyBorder(10,10,10,10));
        fileHistory = new JLabel("None");
        JLabel fileHistoryTitle = new JLabel("Files Added: ");
        fileHistoryTitle.setFont(fileHistoryTitle.getFont().deriveFont(Font.BOLD, 13f));
        fileHistoryPanel.add(fileHistoryTitle);
        fileHistoryPanel.add(fileHistory);
        
        containerPanel.add(buttonPanel, BorderLayout.PAGE_START);
        containerPanel.add(dataPanel, BorderLayout.CENTER);
        containerPanel.add(fileHistoryPanel, BorderLayout.PAGE_END);
        
        chooseButton.addActionListener(
                new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent event)
                    {
                        int returnValue = fc.showOpenDialog(MainFrame);
                        if(returnValue == JFileChooser.APPROVE_OPTION)
                        {
                        		if (fileHistory.getText() == "None")
                        			fileHistory.setText(fc.getSelectedFile().getName());
                        		else
                        			if (fileHistory.getText().length() <= 45)
                        				fileHistory.setText(fc.getSelectedFile().getName() + ", " + fileHistory.getText());
                        			else
                        				fileHistory.setText(fc.getSelectedFile().getName() + ", " + fileHistory.getText().substring(0,45) + " ...");
                        		
                             current = history.addFile(fc.getSelectedFile().getPath());
                             updateCurrentData();
                             updateAggregateData();
                             appendDataToLog();
                        }
                    }
                });
        
        
        helpButton.addActionListener(
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent event)
                {
                    JFrame helpFrame = new JFrame();
                    String helpInfo = "Thank you for using the FileAnalyzer. \n"
                            + "Using the FileAnalyzer is simple: just click the \"Choose a File\" button\n"
                            + "and use the resulting file navigator to select your .txt file anywhere\n"
                            + "on your computer.\n\n"
                            + "The following data will appear:\n"
                            + "The number of lines in the file,\n"
                            + "The number of blank lines in the file,\n"
                            + "The number of spaces in the file,\n"
                            + "The number of words in the file,\n"
                            + "The average number of characters per line in the file,\n"
                            + "The average number of characters per word in the file\n"
                            + "And the three most common words in the file.\n\n";
                    JTextArea helpArea = new JTextArea();
                    helpArea.setText(helpInfo);
                    helpArea.setEditable(false);
                    helpArea.setLineWrap(true);
                    helpFrame.add(new JScrollPane(helpArea),BorderLayout.CENTER);
                    helpFrame.setSize(500,300);
                    helpFrame.setMinimumSize(helpFrame.getSize());
                    helpFrame.setMaximumSize(helpFrame.getSize());
                    helpFrame.setVisible(true);
                }
            });
        
        closeButton.addActionListener(
                new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent event)
                    {
                        System.exit(0);
                    }
                });
    }
    
    private void appendDataToLog()
    {
    			if (current != null) {
                    try
                    {
                        FileWriter write = new FileWriter("log.txt", true);
                        PrintWriter writer = new PrintWriter(write);
                        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                        Date date = new Date();
                        writer.println("ENTRY AT "+dateFormat.format(date));
                        writer.println("Average number of lines: "+history.getAverageNumberOfLines());
                        writer.println("Average number of blank lines: "+history.getAverageNumberOfBlankLines());
                        writer.println("Average number of words: "+history.getAverageNumberOfWords());
                        writer.println("Average number of characters per line: "+history.getAverageCharsPerLine());
                        writer.println("Average number of spaces in each file: "+history.getAverageNumberOfSpaces());
                        writer.println("Average number of characters in each word: "+history.getAverageWordLength());
                        writer.println("Files processed: "+fileHistory.getText());
                        writer.println("\n");
                        writer.close();
                    }
                    catch(Exception e)
                    {   
                        JOptionPane errorPane = new JOptionPane("ERROR: file IO failed!");
                        errorPane.setVisible(true);
                    }
            }
    }
    
    private void updateCurrentData()
    {
    		String currentFileDataText = "<html><br/>";
    	
    		if (current != null) {
	    		
	    		currentFileDataText  	+= "Number of lines: " + current.getNumberOfLines() + "<br/><br/>"
	    								 + "Number of blank lines: " + current.getNumberOfBlankLines() + "<br/><br/>"
	    								 + "Number of spaces: " + current.getNumberOfSpaces() + "<br/><br/>"
	    								 + "Number of words: " + current.getNumberOfWords() + "<br/><br/>"
	    								 + "Average characters per line: " + new DecimalFormat("#.##").format(current.getAverageCharsPerLine()) + "<br/><br/>"
	    								 + "Average word length: " + current.getAverageWordLength() + "<br/><br/>"
	    								 + "Most common words: " + "<br/><br/>";
	    		
	    		ArrayList<String> mostCommonWords = current.getMostCommonWords();
	    		if (mostCommonWords.size() == 0)
	    			currentFileDataText += "There are none  ";
	    		else
	    			for (String word : mostCommonWords)
	    				currentFileDataText += word + ", ";
	    		currentFileDataText = currentFileDataText.substring(0, currentFileDataText.length() - 2);
	    		
    		} else {
    			currentFileDataText += "No files have been added yet";
    		}
    		
    		currentFileDataText += "</html>";
    		currentFileData.setText(currentFileDataText);
    }
    
    private void updateAggregateData()
    {
    		String aggregateFileDataText = "<html><br/>";
    	
		if (current != null) {
	    		aggregateFileDataText 	+= "Number of lines: " + history.getAverageNumberOfLines() + "<br/><br/>"
	    							  	 + "Number of blank lines: " + history.getAverageNumberOfBlankLines() + "<br/><br/>"
	    							  	 + "Number of spaces: " + history.getAverageNumberOfSpaces() + "<br/><br/>"
	    							  	 + "Number of words: " + history.getAverageNumberOfWords() + "<br/><br/>"
	    							  	 + "Average characters per line: " + new DecimalFormat("#.##").format(history.getAverageCharsPerLine()) + "<br/><br/>"
	    							  	 + "Average word length: " + history.getAverageWordLength();
		} else {
			aggregateFileDataText += "No files have been added yet";
		}
		
		aggregateFileDataText += "</html>";
    		aggregateFileData.setText(aggregateFileDataText);
    }
}