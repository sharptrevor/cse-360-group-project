import javax.swing.JFrame;

public class FileAnalyzer {

    public static void main(String[] args) {
        AnalyzerFrame frame = new AnalyzerFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   //client exits on close
        frame.setSize(600, 440);   //program's size is 600 pixels by 460 pixels
        frame.setMinimumSize(frame.getSize());
        frame.setMaximumSize(frame.getSize());
        frame.setVisible(true);   //program is now visible
    }
}