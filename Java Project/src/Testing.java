import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ArrayList;
import java.util.HashMap;
import org.junit.BeforeClass;

public class Testing {
    
    private static ArrayList<TextFile> textFiles = new ArrayList<TextFile>();
    private final static int NUMBER_OF_TESTCASES = 10;
    
    @BeforeClass
    public static void now(){
        
        String datapath = "testcases/testcase";
        
        for (int i = 1; i <= NUMBER_OF_TESTCASES; i++)
        		textFiles.add(new TextFile(datapath + i + ".txt"));
    }
    
    @Test
    public void testGetNumberOfLines() {
        
    		HashMap<Integer, Integer> numberOfLines = new HashMap<Integer, Integer>();
    		numberOfLines.put(1, 4);
    		numberOfLines.put(2, 8);
    		numberOfLines.put(3, 14);
    		numberOfLines.put(4, 15);
    		numberOfLines.put(5, 20);
    		numberOfLines.put(6, 24);
    		numberOfLines.put(7, 27);
    		numberOfLines.put(8, 40);
    		numberOfLines.put(9, 36);
    		numberOfLines.put(10, 40);
    		
    		for (int i = 1; i <= NUMBER_OF_TESTCASES; i++)
    			assertEquals((int) numberOfLines.get(i), textFiles.get(i-1).getNumberOfLines());
    }
    
    @Test
    public void testGetNumberOfBlankLines() {
    	
    		HashMap<Integer, Integer> numberOfBlankLines = new HashMap<Integer, Integer>();
		numberOfBlankLines.put(1, 0);
		numberOfBlankLines.put(2, 2);
		numberOfBlankLines.put(3, 4);
		numberOfBlankLines.put(4, 14);
		numberOfBlankLines.put(5, 5);
		numberOfBlankLines.put(6, 2);
		numberOfBlankLines.put(7, 3);
		numberOfBlankLines.put(8, 4);
		numberOfBlankLines.put(9, 12);
		numberOfBlankLines.put(10, 9);
		
		for (int i = 1; i <= NUMBER_OF_TESTCASES; i++)
			assertEquals((int) numberOfBlankLines.get(i), textFiles.get(i-1).getNumberOfBlankLines());
    }
    
    @Test
    public void testGetNumberOfSpaces() {
    	
    		HashMap<Integer, Integer> numberOfSpaces = new HashMap<Integer, Integer>();
		numberOfSpaces.put(1, 52);
		numberOfSpaces.put(2, 76);
		numberOfSpaces.put(3, 140);
		numberOfSpaces.put(4, 0);
		numberOfSpaces.put(5, 139);
		numberOfSpaces.put(6, 237);
		numberOfSpaces.put(7, 242);
		numberOfSpaces.put(8, 0);
		numberOfSpaces.put(9, 196);
		numberOfSpaces.put(10, 331);
		
		for (int i = 1; i <= NUMBER_OF_TESTCASES; i++)
			assertEquals((int) numberOfSpaces.get(i), textFiles.get(i-1).getNumberOfSpaces());
    }
    
    @Test
    public void testGetNumberOfWords() {
    	
    		HashMap<Integer, Integer> numberOfWords = new HashMap<Integer, Integer>();
		numberOfWords.put(1, 53);
		numberOfWords.put(2, 80);
		numberOfWords.put(3, 148);
		numberOfWords.put(4, 1);
		numberOfWords.put(5, 58);
		numberOfWords.put(6, 248);
		numberOfWords.put(7, 253);
		numberOfWords.put(8, 36);
		numberOfWords.put(9, 209);
		numberOfWords.put(10, 352);
		
		for (int i = 1; i <= NUMBER_OF_TESTCASES; i++)
			assertEquals((int) numberOfWords.get(i), textFiles.get(i-1).getNumberOfWords());
    }
    
    @Test
    public void testGetAverageCharsPerLine() {
    	
    		HashMap<Integer, Double> averageCharsPerLine = new HashMap<Integer, Double>();
		averageCharsPerLine.put(1, 71.5);
		averageCharsPerLine.put(2, 48.75);
		averageCharsPerLine.put(3, 49.14);
		averageCharsPerLine.put(4, 0.31);
		averageCharsPerLine.put(5, 18.15);
		averageCharsPerLine.put(6, 51.0);
		averageCharsPerLine.put(7, 41.74);
		averageCharsPerLine.put(8, 3.15);
		averageCharsPerLine.put(9, 29.80);
		averageCharsPerLine.put(10, 44.07);
		
		for (int i = 1; i <= NUMBER_OF_TESTCASES; i++)
			assertEquals((double) averageCharsPerLine.get(i), textFiles.get(i-1).getAverageCharsPerLine(), 0.1);   
    }
}