import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;

public class TextFile {
    
    private ArrayList<ArrayList<String>> words = new ArrayList<ArrayList<String>>();
    private int numberOfSpaces;
    
    public TextFile(String path){
        
        numberOfSpaces = 0;
        
        // Get the file from the path
        File file = new File(path);
        
        // Scan the file
        Scanner input;
        try {
            input = new Scanner(file);
            
            // Create 2D ArrayList from the contents
            while (input.hasNextLine()) {
                String line = input.nextLine();
                if (line.equals(""))
                    words.add(new ArrayList<String>());
                else
                    words.add(new ArrayList<String>(Arrays.asList(line.split("\\s+"))));
            }
            
            input.close();
            input = new Scanner(file);
            
            while (input.hasNextLine()) {
                String line = input.nextLine();
                for (char c : line.toCharArray())
                    if (c == ' ' || c == '\t')
                        numberOfSpaces++;
            }
            
            input.close();
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    // Return the number of lines
    public int getNumberOfLines() {
        
        //Return size of parent ArrayList
        return words.size();
    }
    
    //Return number of blank lines
    public int getNumberOfBlankLines() {
        
        int count = 0;
        
        //Loop through each line and check if it has any words in it
        for (ArrayList<String> line: words) {
            if (line.size() == 0)
                count++;
        }
        
        return count;
    }
    
    //Return number of spaces in document
    public int getNumberOfSpaces() {
        
        return numberOfSpaces;
    }
    
    //Return number of words in document
    public int getNumberOfWords() {
        
        // create variable to count number of words
        int count = 0;
        
        // get size of each line which is how many words each line has.
        for (ArrayList<String> line: words)
            for (String word: line)
                if (!word.equals(""))
                    count++;
        
        return count;
    }
    
    //Return average characters per line as a double
    public double getAverageCharsPerLine() {
        
        double characters = 0;
        
        //Go through each line
        for (ArrayList<String> line: words)
            //Go through each word on select line
            for (String singleWord: line)
                //Add number of characters on that line to running total
                characters += singleWord.length();
        
        //Error checking for zero
        if (words.size() != 0)
            return characters / words.size();
        else
            return 0;
    }
    
    public int getAverageWordLength() {
        
        int size = 0;
        
        //Loop through each line
        for (ArrayList<String> line: words)
            //Loop through each words
            for (String word: line)
                size += word.length();
        
        //Error checking for zero
        if (getNumberOfWords() != 0)
            return size / getNumberOfWords();
        else
            return 0;
    }
    
    public ArrayList<String> getMostCommonWords() {
        
        ArrayList<String> key = new ArrayList<>();
        ArrayList<Integer> value = new ArrayList<>();
        
        //Loop through each line
        for (ArrayList<String> line: words) {
            
            //Loop through each words
            for (String word: line) {
                
                // if the word has already been counted once, increase its index
                if (key.contains(word)) {
                    
                    int index = key.indexOf(word);
                    int oldCount = value.get(index);
                    int newCount = oldCount += 1;
                    value.remove(index);
                    value.add(index, newCount);
                    
                    //If the word has not been counted, add it to dictionary
                } else {
                    
                    key.add(word);
                    value.add(1);
                }
            }
        }
        
        //keep tracks if word counts tie
        int ties = 0;
        
        //ArrayList to be returned to GUI front-end
        ArrayList <String> mostCommon = new ArrayList<>();
        
        //Placement stores count for each word in decreasing magnitude
        int [] placement = {-1, -2, -3};
        
        //Places stores corresponding indexes for placement values in decreasing magnitude
        int [] places = {-1, -2, -3};
        
        //For loop with size equivalent to one of dictionary ArrayLists
        for (int i = 0; i < key.size(); i++) {
            
            // if count for words is larger than or equal to current count at head of placement
            // array, add the new count and corresponding index
            if (value.get(i) >= placement[0]) {
                
                //If any counts added equal previous counts, flag it
                if(value.get(i) == placement[0])
                    ties++;
                
                //Add new count to placement and shift everything to right
                placement[2]=placement[1];
                placement[1]=placement[0];
                placement[0] = value.get(i);
                
                //Add new index to places and shift everything to right
                places[2]=places[1];
                places[1]=places[0];
                places[0] = i;
            }
        }
        
        //Calculate total number of words and total count of all word counts
        int totalWords = getNumberOfWords();
        int totalCount = places[0] + places[1] + places[2];
        
        //If total count for each word is equal to all words, than all words are most common
        // as they are only words present
        if (totalCount == totalWords) {
            mostCommon.add(key.get(places[0]));
            mostCommon.add(key.get(places[1]));
            mostCommon.add(key.get(places[2]));
            
            // If there were no ties and all counts were unique, grab nonnegative
            // indexes and use to save value to mostCommon ArrayList
        } else if(ties >= 0 && ties <= 2) {
            
            for (int i = 0; i < 3; i++)
                if (places[i] >= 0)
                    mostCommon.add(key.get(places[i]));
            
            //There were ties and totalCount != totalWords implying words that tied
        } else if(ties > 2) {
            
            if (placement[2] == placement[1])
                if(placement[0] == placement[1])
                    return null;
                else
                    mostCommon.add(key.get(places[0]));
        }
        
        return mostCommon;
    }
}