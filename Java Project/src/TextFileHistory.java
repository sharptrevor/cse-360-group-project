import java.util.ArrayList;

public class TextFileHistory {
    
    private ArrayList<TextFile> files = new ArrayList<TextFile>();
    
    public TextFile addFile(String path) {
        files.add(0, new TextFile(path));
        return files.get(0);
    }
    
    public int getAverageNumberOfLines() {
        int totalNumberOfLines = 0;
        for (TextFile file: files)
            totalNumberOfLines += file.getNumberOfLines();
        
        if (files.size() > 0)
            return totalNumberOfLines / files.size();
        else
            return 0;
    }
    
    public int getAverageNumberOfBlankLines() {
        int totalNumberOfBlankLines = 0;
        for (TextFile file: files)
            totalNumberOfBlankLines += file.getNumberOfBlankLines();
        
        if (files.size() > 0)
            return totalNumberOfBlankLines / files.size();
        else
            return 0;
    }
    
    public int getAverageNumberOfSpaces() {
        int totalNumberOfSpaces = 0;
        for (TextFile file: files)
            totalNumberOfSpaces += file.getNumberOfSpaces();
        
        if (files.size() > 0)
            return totalNumberOfSpaces / files.size();
        else
            return 0;
    }
    
    public int getAverageNumberOfWords() {
        int totalNumberOfWords = 0;
        for (TextFile file: files)
            totalNumberOfWords += file.getNumberOfWords();
        
        if (files.size() > 0)
            return totalNumberOfWords / files.size();
        else
            return 0;
    }
    
    public double getAverageCharsPerLine() {
        int totalNumberOfChars = 0;
        int totalNumberOfLines = 0;
        for (TextFile file: files) {
            totalNumberOfLines += file.getNumberOfLines();
            totalNumberOfChars += file.getAverageCharsPerLine() * file.getNumberOfLines();
        }
        
        if (totalNumberOfLines > 0)
            return totalNumberOfChars / totalNumberOfLines;
        else
            return 0;
    }
    
    public int getAverageWordLength() {
        int totalNumberOfChars = 0;
        int totalNumberOfWords = 0;
        for (TextFile file: files) {
            totalNumberOfWords += file.getNumberOfWords();
            totalNumberOfChars += file.getAverageWordLength() * file.getNumberOfWords();
        }
        
        if (totalNumberOfWords > 0)
            return totalNumberOfChars / totalNumberOfWords;
        else
            return 0;
    }
}

